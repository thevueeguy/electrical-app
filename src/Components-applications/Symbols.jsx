import React from 'react'
import { useState, useEffect } from 'react'
import { Addnew } from './Addnew'
import './css/formula.css'
import { Link } from 'react-router-dom'
import AddedList from './AddedList'


const Symbols = () => {
  return (
    <div className='formulaes'>
      <ul>
        <li ><Link className='lists' to='/ohm'></Link></li>
        <li ><Link className='lists' to='/series-parallel'></Link></li>
        <li ><Link className='lists' to='/singlephase'></Link></li>
        <li ><Link className='lists' to='/3phases'></Link></li>
        <li ><Link className='lists' to='/conversion'></Link></li>
      </ul>
        <Link to='/create'> <Addnew/> </Link>
    </div>
  )
}

export default Symbols