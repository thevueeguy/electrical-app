import React from 'react'
import { Link } from 'react-router-dom'
import './css/formula.css'

const Calculator = () => {
  return (
    <div className="formulaes">
      <ul>
        <li>
          <Link className="lists oh" to="/ohmsCalc">
            Ohm's Law
          </Link>
        </li>
        <li>
          <Link className="lists se" to="/series-parallel">
            Series-Parallel
          </Link>
        </li>
        <li>
          <Link className="lists si" to="/singlephase">
            Single Phase
          </Link>
        </li>
        <li>
          <Link className="lists th" to="/3phases">Three Phase</Link>
        </li>
        <li>
          <Link className="lists co" to="/conversion">Conversion</Link>
        </li>
      </ul>
    </div>
  )
}

export default Calculator
