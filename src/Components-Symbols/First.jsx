import React from 'react';
import first from './1.jpg';

const First = () => {
  return (
    <div>
        <img src={first} alt="" />
    </div>
  )
}

export default first