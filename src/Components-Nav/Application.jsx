import React from 'react'
import { Link } from 'react-router-dom'
import '../css/style.css'
import dict from '../img/dict.png'
import formula from '../img/formula.png'
import cal from '../img/cal.png'
import symbol from '../img/symbol.png'

export const Application = () => {
	return (
		<div>
			<section className="section-tours" id="section-tours">
				<div className="u-center-text u-margin-bottom-big">
					<h2 className="heading-secondary">Application Section</h2>
				</div>

				<div className="row">
					<div className="col-1-of-3">
						<div className="card">
							<div className="card__side card__side--front">
								<div className="card__picture card__picture--1">&nbsp;</div>
								<h4 className="card__heading">
									<span className="card__heading-span card__heading-span--1">
										Formulas
									</span>
								</h4>
								<div className="card__details">
									<ul>
										<li>Learn new things.</li>
										<li>Grow together</li>
										<li>Think out of the box</li>
										<li>Love our application</li>
										<li>All the best to you</li>
									</ul>
								</div>
							</div>
							<div className="card__side card__side--back card__side--back-1">
							<div className="card__cta">
									<img src={formula} alt="" />
									<Link to="/Formula" className="btn btn--white">
										Enter!
									</Link>
								</div>
							</div>
						</div>
					</div>

					<div className="col-1-of-3">
						<div className="card">
							<div className="card__side card__side--front">
								<div className="card__picture card__picture--2">&nbsp;</div>
								<h4 className="card__heading">
									<span className="card__heading-span card__heading-span--2">
										Dictionary
									</span>
								</h4>
								<div className="card__details">
								<ul>
										<li>Learn new things.</li>
										<li>Grow together</li>
										<li>Think out of the box</li>
										<li>Love our application</li>
										<li>All the best to you</li>
									</ul>
								</div>
							</div>
							<div className="card__side card__side--back card__side--back-2">
								<div className="card__cta">
									<img src={dict} alt="" />
									<Link to="/Dictionary" className="btn btn--white">
										Enter!
									</Link>
								</div>
							</div>
						</div>
					</div>

					<div className="col-1-of-3">
						<div className="card">
							<div className="card__side card__side--front">
								<div className="card__picture card__picture--3">&nbsp;</div>
								<h4 className="card__heading">
									<span className="card__heading-span card__heading-span--3">
										Calculator
									</span>
								</h4>
								<div className="card__details">
								<ul>
										<li>Learn new things.</li>
										<li>Grow together</li>
										<li>Think out of the box</li>
										<li>Love our application</li>
										<li>All the best to you</li>
									</ul>
								</div>
							</div>
							<div className="card__side card__side--back card__side--back-3">
							<div className="card__cta">
									<img src={cal} alt="" />
									<Link to="/Calculator" className="btn btn--white">
										Enter!
									</Link>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div className="row-2-of-2">
					<div className="card">
						<div className="card__side card__side--front">
							<div className="card__picture card__picture--4">&nbsp;</div>
							<h4 className="card__heading">
								<span className="card__heading-span card__heading-span--4">
									Symbols
								</span>
							</h4>
							<div className="card__details">
							<ul>
										<li>Learn new things.</li>
										<li>Grow together</li>
										<li>Think out of the box</li>
										<li>Love our application</li>
										<li>All the best to you</li>
									</ul>
							</div>
						</div>
						<div className="card__side card__side--back card__side--back-4">
						<div className="card__cta">
									<img src={symbol} alt="" />
									<Link to="/Symbols" className="btn btn--white">
										Enter!
									</Link>
								</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	)
}
