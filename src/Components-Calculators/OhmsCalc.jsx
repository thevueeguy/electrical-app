import React, { useState } from 'react'
import './css/ohmsCalc.css'
import '../css/style.css'
const OhmsCalc = () => {
	const [Volt, setVolt] = useState('')
	const [Current, setCurrent] = useState('')
	const [Resistance, setResistance] = useState('')
	const [value, setValue] = useState('') 
	const [Power, setPower] = useState('') 

	function handleChange(e) {
		setValue(e.target.value)
	}

	function handleVoltage(e) {
		e.preventDefault()
		setVolt(Current * Resistance)
	}

  function handleCurrent(e) {
		e.preventDefault()
		setVolt(Volt * Resistance)
	}
  function handleResistance(e) {
		e.preventDefault()
		setVolt(Volt * Current)
	}
  function handlePower(e) {
		e.preventDefault()
		setVolt(Volt * Current)
	}

	return (
		<div className="section-book containing">
			<div className='firstDiv'>
				<tr>
					<td>
						<p>Ohm's Law for : </p>
					</td>
					<td>
						<select
							defaultValue={value}
							className="optionss"
							onChange={handleChange}>
							<option value="" hidden></option>
							<option value="Voltage">Voltage</option>
							<option value="Current">Current</option>
							<option value="Resistance">Resistance</option>
							<option value="Power">Power</option>
						</select>
					</td>
				</tr>
			</div>

			{value === 'Voltage' && (
				<form className="formOfCreate" onSubmit={handleVoltage}>
					<div>
						<tr>
							<td>
								<label>Current(I) : </label>
							</td>
							<td>
								<input
									type="text"
									required
									value={Current}
									onChange={(e) => setCurrent(e.target.value)}
								/>
							</td>
						</tr>
						<td>
							<label>Resistance : </label>
						</td>
						<td>
							<input
								type="text"
								required
								value={Resistance}
								onChange={(e) => setResistance(e.target.value)}
							/>
						</td>
					</div>
					<button className="btn btn--white btn--animated">Calculate</button>
					{Volt}
				</form>
			)}
			{value === 'Current' && (
				<form className="formOfCreate" onSubmit={handleCurrent}>
					<div>
						<tr>
							<td>
								<label>Voltage(V) : </label>
							</td>
							<td>
								<input
									type="text"
									required
									value={Volt}
									onChange={(e) => setCurrent(e.target.value)}
								/>
							</td>
						</tr>
						<td>
							<label>Resistance : </label>
						</td>
						<td>
							<input
								type="text"
								required
								value={Resistance}
								onChange={(e) => setResistance(e.target.value)}
							/>
						</td>
					</div>
					<button className="btn btn--white btn--animated">Calculate</button>
					{Current}
				</form>
			)}
			{value === 'Resistance' && (
				<form className="formOfCreate" onSubmit={handleResistance}>
					<div>
						<tr>
							<td>
								<label>Current(I) : </label>
							</td>
							<td>
								<input
									type="text"
									required
									value={Current}
									onChange={(e) => setCurrent(e.target.value)}
								/>
							</td>
						</tr>
						<td>
							<label>Resistance : </label>
						</td>
						<td>
							<input
								type="text"
								required
								value={Resistance}
								onChange={(e) => setResistance(e.target.value)}
							/>
						</td>
					</div>
					<button className="btn btn--white btn--animated">Calculate</button>
					{Resistance}
				</form>
			)}
			{value === 'Power' && (
				<form className="formOfCreate" onSubmit={handlePower}>
					<div>
						<tr>
							<td>
								<label>Voltage(V) : </label>
							</td>
							<td>
								<input
									type="text"
									required
									value={Volt}
									onChange={(e) => setCurrent(e.target.value)}
								/>
							</td>
						</tr>
						<td>
							<label>Current : </label>
						</td>
						<td>
							<input
								type="text"
								required
								value={Current}
								onChange={(e) => setResistance(e.target.value)}
							/>
						</td>
					</div>
					<button className="btn btn--white btn--animated">Calculate</button>
					{Power}
				</form>
			)}
		</div>
	)
}

export default OhmsCalc
